<!--
     # WEBSITE PROJECT � TEAM151 LTD DEPLOY PLATFORM #
     I/O: Website Project
     Machine: Team151 LTD Deploy Platform
     Type: CMS THEME
     CMS: Wordpress
     Author: Team151 LTD Members
     Slogan: The Easy Solution for your business and marketing.
     Description: This project comes from the minds of the team151 ltd,
                  a British company that thinks big and wants to give all
                  the new shops and all the new companies and manufacturers,
                  the ability to easily have an online visibility and to
                  enter their fashion on the market faster , in a simple
                  professional way and at a very low cost.
                                                                         -->

<!DOCTYPE html>
<html lang="en"/>
<head>
    <!-- META Information -->
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <!-- Font & Image -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
          rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"/>
    <link rel="icon" type="image/png" href="http://178.62.9.13:7123/repo/papl_wp_t151/img//favicon/favicon.png"/>
    <link rel="stylesheet" href="assets/bootstrap4.0/bootstrap.min.css"/>
    <link rel="stylesheet" href="assets/private/style.css"/>
    <!-- SEO Tag -->
    <title>Papillon Theme // For Your Professional</title>
    <meta name="description" content="Papillon Theme - By Team151 LTD">
    <meta name="author" content="Team151 LTD">
</head>

<body>
<!-- NAVBAR STARTS -->
<nav class="navbar sticky-top navbar-expand-lg navbar-light nvb-check fadeInUp animated">
    <div class="container">
        <!-- NAVBAR BRAND -->
        <a class="navbar-brand" href="#">
            <img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/favicon/favicon.png" width="35" alt="">
        </a>
        <button class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- NAVBAR MENU -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link fix-a-link" href="#"><i id="fix-icn" class="fas fa-male"></i>
                        Male
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" id="spra" href="#">|</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link fix-a-link" href="#"><i id="fix-icn" class="fas fa-female"></i>
                        Female
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" id="spra" href="#">|</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link fix-a-link" href="#"><i id="fix-icn" class="fas fa-child"></i>
                        Child
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" id="spra" href="#">||</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link fix-a-link" href="#"><i id="fix-icn" class="far fa-newspaper"></i>
                        Newsletter
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" id="spra" href="#">||</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link fix-a-link sp-red" href="#">
                        <i id="fix-icn" class="fas fa-tag"></i>
                        Sale
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-link" id="spra" href="#">||</span>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i id="sp-14" class="fas fa-shopping-bag sp-blue"></i>
                        <span class="sr-only">
                  (current)
                </span>
                    </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="#"><i id="sp-14" class="fas fa-user sp-blue"></i>
                        <span class="sr-only">
                  (current)
                </span>
                        <span class="usr-frmtd">
                  Charlotte
                </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- NAVBAR ENDS -->

<!-- COVER & SLIDER START -->
<div class="container-fluid fix-cnt-fld">
    <!-- Carousel Start -->
    <!-- Carousel Indicator -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <!-- Carousel Inner -->
        <div class="carousel-inner">
            <!-- Carousel Item Initial -->
            <div class="carousel-item active bck fix-cl-it">
                <img class="d-block w-100" src="..." alt="">
                <div class="container">
                    <div class="main-content">
                        <!-- HEADER WITH TITLE AND SUBTITLE -->
                        <div class="papillon-title-header fix-tl-hr">
                            <div class="papillon-title fadeInUp animated">
                                <button id="br0" type="button" class="btn btn-light">
                                    <!-- Carousel Title -->
                                    <h1 class="cvr-title">
                                        Papillon Theme
                                    </h1>
                                </button>
                            </div>
                            <div class="papillon-subtitle an-dl03 fadeInUp animated">
                                <button id="br0" type="button" class="btn btn-light">
                                    <h3 class="cvr-prg">
                                        For your professional
                                    </h3>
                                </button>
                            </div>
                        </div>

                        <!-- DESCRIPTION PARAGRAPH -->
                        <div class="papl-desc">
                            <div class="paragraph-title an-dl03 fadeInUp animated">
                                <button type="button" class="btn btn-dark fix-par-tl">
                                    <h3 class="desc-box-tl">
                                        Breadcrumb
                                    </h3>
                                </button>
                            </div>
                            <div class="paragraph-body an-dl10 fadeInUp animated">
                                <p class="fix-par-desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                    reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                    culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel Item End -->
            <!-- Carousel Item Initial -->
            <div class="carousel-item bck2 fix-cl-it">
                <img class="d-block w-100" src="..." alt="">
                <div class="container">
                    <div class="main-content">
                        <!-- HEADER WITH TITLE AND SUBTITLE -->
                        <div class="papillon-title-header fix-tl-hr">
                            <div class="papillon-title fadeInUp animated">
                                <button id="br0" type="button" class="btn btn-light">
                                    <!-- Carousel Title -->
                                    <h1 class="cvr-title">
                                        Papillon Theme
                                    </h1>
                                </button>
                            </div>
                            <div class="papillon-subtitle an-dl03 fadeInUp animated">
                                <button id="br0" type="button" class="btn btn-light">
                                    <h3 class="cvr-prg">
                                        For your professional
                                    </h3>
                                </button>
                            </div>
                        </div>

                        <!-- DESCRIPTION PARAGRAPH -->
                        <div class="papl-desc">
                            <div class="paragraph-title an-dl03 fadeInUp animated">
                                <button type="button" class="btn btn-dark fix-par-tl">
                                    <h3 class="desc-box-tl">
                                        Breadcrumb
                                    </h3>
                                </button>
                            </div>
                            <div class="paragraph-body an-dl10 fadeInUp animated">
                                <p class="fix-par-desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                    reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                    culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel Item End -->
            <!-- Carousel Item Initial -->
            <div class="carousel-item bck3 fix-cl-it">
                <img class="d-block w-100" src="..." alt="">
                <div class="container">
                    <div class="main-content">
                        <!-- HEADER WITH TITLE AND SUBTITLE -->
                        <div class="papillon-title-header fix-tl-hr">
                            <div class="papillon-title fadeInUp animated">
                                <button id="br0" type="button" class="btn btn-light">
                                    <!-- Carousel Title -->
                                    <h1 class="cvr-title">
                                        Papillon Theme
                                    </h1>
                                </button>
                            </div>
                            <div class="papillon-subtitle an-dl03 fadeInUp animated">
                                <button id="br0" type="button" class="btn btn-light">
                                    <h3 class="cvr-prg">
                                        For your professional
                                    </h3>
                                </button>
                            </div>
                        </div>

                        <!-- DESCRIPTION PARAGRAPH -->
                        <div class="papl-desc">
                            <div class="paragraph-title an-dl03 fadeInUp animated">
                                <button type="button" class="btn btn-dark fix-par-tl">
                                    <h3 class="desc-box-tl">
                                        Breadcrumb
                                    </h3>
                                </button>
                            </div>
                            <div class="paragraph-body an-dl10 fadeInUp animated">
                                <p class="fix-par-desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                    nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                                    reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                                    culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Carousel Item End -->
            <!-- Carousel Control -->
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!-- COVER & SLIDER END -->

    <!-- EVIDENCE BOX START -->
    <div class="container-fluid fix-cnt-fldps">
        <div class="container">
            <div class="papl-box">
                <div class="papl-txt-tl">
                    <p class="papl-txt-dep">
                        // In Evidence &#8226;
                    </p>
                </div>
                <div class="papl-body">
                    <p>
                        For your work
                        <br/>
                        but easy and professional...
                    </p>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-5 text-md-left mb-4 mb-md-0 fix-layout">
                    <div class="papillon-img">
                        <img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/general/img5.jpg" alt="papl-img5">
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 text-md-left mb-4 mb-md-0 fix-col2">
                    <div class="papl-text-cnt">
                        <button type="button" class="btn btn-dark">
                            Your Dress
                        </button>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Excepteur sint occaecat cupidatat non proident, sunt in
                            culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <button type="button" class="btn btn-dark">
                            Elegance Here
                        </button>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna
                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Duis aute irure dolor in reprehenderit in voluptate velit
                            esse cillum dolore eu fugiat nulla pariatur. Duis aute
                            irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint
                            occaecat cupidatat non proident, sunt in culpa qui officia
                            deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="papl-bar">
            <div class="container">
                <div class="row py-4 d-flex align-items-center">
                    <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                        <h6 class="mb-0 black-text">
                  <span class="badge badge-secondary">
                    New
                  </span>
                            Discover New Collection Autumn 2018!
                        </h6>
                    </div>
                    <div class="col-md-6 col-lg-7 text-center text-md-right">
                        <button type="button" class="btn btn-primary">
                            Collection
                        </button>
                        <button type="button" class="btn btn-outline-primary">
                            Call We
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- EVIDENCE BOX END -->

    <!-- MODELPHOTO BOX START -->
    <div class="container-fluid papl-mdl-pht">
        <div class="container papl-mdl-lyt">
            <h4 class="text-center">
                <i class="far fa-heart"></i>
            </h4>
            <h3 class="text-center">
                "Fashion is not something that exists only in clothes.
                Fashion is in the sky, in the street, fashion has to do
                with ideas, our way of life, what is happening."
            </h3>
            <p class="text-right">
                "Citation. Coco Chanel"
            </p>
            <div class="row d-flex align-items-center">
                <div class="col"><img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/general/img1.jpg"/></div>
                <div class="col"><img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/general/img2.jpg"/></div>
                <div class="w-100"></div>
                <div class="col"><img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/general/img3.jpg"/></div>
                <div class="col"><img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/general/img4.jpg"/></div>
            </div>
        </div>
    </div>
    <!-- MODELPHOTO BOX END -->

    <!-- BIGSOLUTION BOX START -->
    <!-- Text Bar Settings -->
    <div class="container-fluid fix-cnt-lyt">
        <hr/>
        <div class="container" style="padding: 50px 0px 0px 0px;">
            <h2 class="text-center papl-textbar"><i style="color: #FFFFFF;" class="fas fa-cut"></i></h2>
            <h2 class="text-center papl-textbar2">
                "Fools invent fashions and essays follow them."</h2>
        </div>
        <!-- Box Solution Settings -->
        <div class="container-fluid fix-cnt-fldps box-solutions">
            <div class="container">
                <div class="papl-box">
                    <div class="papl-txt-tl">
                        <p class="papl-txt-dep papl-txt-light">
                            // Boxed &#8226;
                        </p>
                    </div>
                    <div class="papl-body papl-txt-light">
                        <p>
                            Lorem ipsum dolor
                            <br/>
                            sit amet consectetur adipisicing.
                        </p>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-5 text-md-left mb-4 mb-md-0 fix-layout">
                        <div class="papillon-img papillon-img2">
                            <img src="http://178.62.9.13:7123/repo/papl_wp_t151/img/general/img6.jpg"
                                 alt="papl-img5">
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 text-md-left mb-4 mb-md-0 fix-col2">
                        <div class="papl-text-cnt">
                            <button type="button" class="btn btn-dark papl-txt-light papl-btn">
                                Your Dress
                            </button>
                            <p class="papl-txt-light">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Excepteur sint occaecat cupidatat non proident, sunt in
                                culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <button type="button" class="btn btn-dark papl-txt-light papl-btn">
                                Elegance Here
                            </button>
                            <p class="papl-txt-light">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                sed do eiusmod tempor incididunt ut labore et dolore magna
                                aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit
                                esse cillum dolore eu fugiat nulla pariatur. Duis aute
                                irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                occaecat cupidatat non proident, sunt in culpa qui officia
                                deserunt mollit anim id est laborum.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- EVIDENCE BOX END -->
        <!-- BIGSOLUTION BOX START -->


        <!-- NEWSLETTER BOX START -->
        <div class="papl-bar-inverted">
            <div class="container">
                <div class="row py-4 d-flex align-items-center">
                    <div class="col-md-8 col-lg-8 text-center text-md-left mb-4 mb-md-0">
                        <h6 class="mb-0 black-text papl-txt-light">
                  <span class="badge badge-light" style="margin-bottom: 7px;">
                    Newsletter
                  </span><br/>
                            See first new <span style="color: #DC3545; padding: 0px 1px 0px 2px; font-size: 15px;"><i
                                class="far fa-snowflake"></i></span> Christmas Collection 2018, and more other
                            information, subscribe now!
                        </h6>
                    </div>
                    <div class="col-md-4 col-lg-4 text-center text-md-right">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Your Email" aria-label="Your Email"
                                   aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-light">
                                    Subscribe
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- NEWSLETTER BOX END -->
    </div>
    <!-- FOOTER START -->
    <footer class="page-footer font-small stylish-color-dark fix-ftr">
        <div class="container text-center text-md-left">
            <div class="row">
                <div class="col-md-4">
                    <h5 class="text-uppercase mb-4 mt-3 font-weight-bold" style="color: #FCFCFC; font-size: 17px;">
                        <div style="background-color: #007BFF; border-radius: 100%; padding: 10px; width: 32px; height: 32px;">
                            <i style="font-size: 23px;" class="fas fa-credit-card"></i></div>
                        <br/>Go on the market!!
                    </h5>
                    <p style="color: #8c8c8c; font-size: 12px;">This project comes from the minds of the team151 ltd, a
                        British company that thinks big and wants to give all the new shops and all the new companies
                        and manufacturers, the ability to easily have an online visibility and to enter their fashion on
                        the market faster , in a simple professional way and at a very low cost.</p>
                </div>
                <hr class="clearfix w-100 d-md-none">
                <div class="col-md-2 mx-auto">
                    <h5 class="text-uppercase mb-4 mt-3 font-weight-bold" style="color: #007BFF; font-size: 13px;"><p
                            style="margin: 0px 0px 3px 0px;"><i
                            style="font-size: 20px; color: #2E2E2E; margin: 5px 0px 0px 0px;" class="fas fa-code"></i>
                    </p><br/>System
                    </h5>
                    <ul class="list-unstyled" style="font-size: 13px;">
                        <li>
                            <a href="#!" style="color: #FFFFFF;">License</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">Legal Notes</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">Clouding</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">API</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">FAQ</a>
                        </li>
                    </ul>
                </div>
                <hr class="clearfix w-100 d-md-none">
                <div class="col-md-2 mx-auto">
                    <h5 class="text-uppercase mb-4 mt-3 font-weight-bold" style="color: #007BFF; font-size: 13px;"><p
                            style="margin: 0px 0px 3px 0px;"><i
                            style="font-size: 20px; color: #2E2E2E; margin: 5px 0px 0px 0px;" class="fas fa-user"></i>
                    </p><br/>Help
                    </h5>
                    <ul class="list-unstyled" style="font-size: 13px;">
                        <li>
                            <a href="#!" style="color: #FFFFFF;">Bookshelf</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">Shop Solution</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">Method Payments</a>
                        </li>
                    </ul>
                </div>
                <hr class="clearfix w-100 d-md-none">
                <div class="col-md-2 mx-auto">
                    <h5 class="text-uppercase mb-4 mt-3 font-weight-bold" style="color: #007BFF; font-size: 13px;"><p
                            style="margin: 0px 0px 3px 0px;"><i
                            style="font-size: 20px; color: #2E2E2E; margin: 5px 0px 0px 0px;"
                            class="far fa-address-card"></i></p><br/>About #151
                    </h5>
                    <ul class="list-unstyled" style="font-size: 13px;">
                        <li>
                            <a href="#!" style="color: #FFFFFF;">www.team151.com</a>
                        </li>
                        <li>
                            <a href="#!" style="color: #FFFFFF;">Legal DMCA</a>
                        </li>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <hr style="background-color: #2E2E2E; margin: 35px 0px 0px 0px;">
        <div class="text-center py-3">
            <ul class="list-unstyled list-inline mb-0">
                <li class="list-inline-item">
                    <h5 class="mb-1" style="color: #FFFFFF; font-size: 17px;">BUY THIS THEME WITH 25% OFF SALE! </h5>
                </li>
                <li class="list-inline-item">
                    <a href="#!" class="btn btn-danger btn-rounded"
                       style="background-color: #007BFF; border: 1px solid #007BFF; position: relative; bottom: 2px;">Buy
                        Now!</a>
                </li>
            </ul>
        </div>
        <hr style="background-color: #2E2E2E; margin: 0px 0px 30px 0px;">
        <div class="text-center">
            <ul class="list-unstyled list-inline">
                <li class="list-inline-item">
                    <a class="btn-floating btn-sm btn-fb mx-1">
                        <i style="color: #FFFFFF;" class="fab fa-facebook-f"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-sm btn-tw mx-1">
                        <i style="color: #FFFFFF;" class="fab fa-twitter"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-sm btn-gplus mx-1">
                        <i style="color: #FFFFFF;" class="fab fa-instagram"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-sm btn-li mx-1">
                        <i style="color: #FFFFFF;" class="fab fa-github"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-sm btn-dribbble mx-1">
                        <i style="color: #FFFFFF;" class="fas fa-globe"> </i>
                    </a>
                </li>
            </ul>
        </div>
        <!--/.Social buttons-->

        <!--Copyright-->
        <div class="footer-copyright py-3 text-center">
            <p style="font-family: 'Roboto',sans-serif; margin: 0px; color: #FFFFFF; font-weight: 200; font-size: 17px;">
                � 2018 Papillon Theme</p>
            <p style="font-size: 12px; color: #8c8c8c; margin: 0px; font-weight: 100; font-family: 'Roboto',sans-serif;">
                Theme created by Team151 LTD, this theme is under copyright from Digital<br/>Millenium Copyrights Act
                2018, all right is reserved.</p>
            <span style="color: #212121; font-weight: 300; position: relative; top: 10px; font-size: 10px; border-radius: 3px; padding: 3px 5px; background-color: #F1F1F1;">By team151.com</span>
        </div>
        <!--/.Copyright-->

    </footer>
    <!-- FOOTER END -->


</div>
</div>
<!-- SCRIPTS AREA START -->
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"
        integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ"
        crossorigin="anonymous">
</script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous">
</script>
</body>
</html>
